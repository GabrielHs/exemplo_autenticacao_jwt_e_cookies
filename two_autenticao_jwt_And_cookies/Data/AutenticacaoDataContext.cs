﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using two_autenticao_jwt_And_cookies.Models;

namespace two_autenticao_jwt_And_cookies.Data
{
    public class AutenticacaoDataContext : DbContext
    {
        public AutenticacaoDataContext(DbContextOptions<AutenticacaoDataContext> options) : base(options) { }

        public DbSet<UserModel> Users { get; set; }



    }
}
