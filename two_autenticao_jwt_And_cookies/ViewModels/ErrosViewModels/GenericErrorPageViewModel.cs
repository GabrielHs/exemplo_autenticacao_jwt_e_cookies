﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace two_autenticao_jwt_And_cookies.ViewModels.ErrosViewModels
{
    public class GenericErrorPageViewModel
    {

        public int? HttpErrorCode { get; set; }

        public string Nome { get; set; }

        public string Mensagem { get; set; }


    }
}
