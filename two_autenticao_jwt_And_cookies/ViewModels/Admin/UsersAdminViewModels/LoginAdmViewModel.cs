﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using two_autenticao_jwt_And_cookies.Controllers;

namespace two_autenticao_jwt_And_cookies.ViewModels.Admin.UsersAdminViewModels
{
    public class LoginAdmViewModel
    {
        [Required(ErrorMessage = "Nome é obrigatorio")]
        [Display(Name = "Nome")]
        public string Nome { get; set; }

        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Senha é obrigatorio")]
        [Display(Name = "Senha")]
        public string Password { get; set; }

        public string UrlRetorno { get; set; }





        public JsonResult ValidacaoBackend(BaseController gabrielController)
        {

            bool existeUsuario = gabrielController.Db.Users.FirstOrDefault(u => u.Nome == Nome) == null ? false : true ;

            if (!existeUsuario)
            {
                gabrielController.ModelState.AddModelError(nameof(Nome), "Desculpe usuario informado não existe, realize um cadastro");
            }

            return null;
        }


    }
}
