﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace two_autenticao_jwt_And_cookies.ViewModels.Api.UsersViewModels
{
    public class CriacaoUsuarioViewModel
    {
        [Required]
        public string Nome { get; set; }

        [Required]
        public string TipoRole { get; set; }

        [Required]
        public string Password { get; set; }

    }

}
