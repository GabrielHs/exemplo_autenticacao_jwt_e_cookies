﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using two_autenticao_jwt_And_cookies.Data;
using two_autenticao_jwt_And_cookies.Models;
using two_autenticao_jwt_And_cookies.Responses.LoginResponses;
using two_autenticao_jwt_And_cookies.Utils.Services;
using two_autenticao_jwt_And_cookies.ViewModels.Api.UsersViewModels;

namespace two_autenticao_jwt_And_cookies.Controllers.Api
{
    [ApiController]
    [Route("api/v1")]

    public class UsersController : Controller
    {
        private readonly AutenticacaoDataContext _cxt;
        public UsersController(AutenticacaoDataContext context)
        {
            _cxt = context;
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("users/criacao")]
        public IActionResult CriarUsuario([FromBody] CriacaoUsuarioViewModel model)
        {
            try
            {



                UserModel tbUser = new UserModel()
                {
                    Nome = model.Nome,
                    Role = model.TipoRole,
                };

                var passwordHasher = new PasswordHasher<UserModel>();

                var hashedPassword = passwordHasher.HashPassword(tbUser, model.Password);

                tbUser.Password = hashedPassword;

                _cxt.Users.Add(tbUser);

                _cxt.SaveChanges();

                return Ok("Usuario criado com sucesso");

            }
            catch (Exception ex)
            {

                return BadRequest("Usuario não foi possivel ser criado " + ex.Message);

            }

        }

        [AllowAnonymous]
        [HttpPost]
        [Route("users/login")]
        public IActionResult LoginUserApi([FromBody] LoginUserApiViewModel model)
        {

            try
            {

                var usuario = _cxt.Users.FirstOrDefault(us => us.Nome == model.NomeUsuario) ;

                if (usuario == null)
                {
                    return BadRequest("não encontrado usuario");
                }



                var passwordHasher = new PasswordHasher<UserModel>();

                var hashedPassword = passwordHasher.VerifyHashedPassword(usuario, usuario.Password, model.SenhaUsuario);

                if (hashedPassword != PasswordVerificationResult.Success)
                {
                    return BadRequest("Senha está incorreto");
                }

                var response = new LoginResponse()
                {
                    Nome = usuario.Nome,
                    Role = usuario.Role,
                    Jwt = TokenService.GenerateToken(usuario)
                };



                return Json(response);
            }
            catch (Exception ex)
            {

                return Json("erro ao logar " + ex.Message);
            }
        }




        [HttpGet]
        [Route("autenticado")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public string Authenticated() => String.Format("Autenticado - {0}", User.Identity.Name);

        [HttpGet]
        [Route("permisao/role-dev")]
        //[Authorize(Roles = "desenvolvedor")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "desenvolvedor")]
        public string Employee() => "Desenvolvedor";

        [HttpGet]
        [Route("permisao/role-operador")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "operador")]
        //[Authorize(Roles = "operador")]
        public string Employee2() => "Operador";



    }
}
