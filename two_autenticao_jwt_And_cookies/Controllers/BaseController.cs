﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using two_autenticao_jwt_And_cookies.Data;

namespace two_autenticao_jwt_And_cookies.Controllers
{
    public class BaseController : Controller
    {

        protected readonly AutenticacaoDataContext db;

        public AutenticacaoDataContext Db => db;
        public BaseController(AutenticacaoDataContext context)
        {

            this.db = context;
        }
    }
}
