﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using two_autenticao_jwt_And_cookies.ViewModels.ErrosViewModels;

namespace two_autenticao_jwt_And_cookies.Controllers
{
    [AllowAnonymous]
    public class ErroController : Controller
    {
        [Route("[Controller]/{httpStatusCode:int?}")]
        public IActionResult Erro(int? httpStatusCode = null)
        {
            var model = new GenericErrorPageViewModel
            {
                HttpErrorCode = Response.StatusCode
            };

            if (httpStatusCode == 401)
            {
                model.HttpErrorCode = httpStatusCode;
            }

            switch (model.HttpErrorCode)
            {
                case 404:
                    model.Nome = "Página não encontrada";
                    model.Mensagem = "Oops! Pedimos desculpas, mas a página que você está procurando não pôde ser encontrada.";
                    break;

                case 400:
                    model.Nome = "Solicitação inválida";
                    model.Mensagem = "Pedimos desculpas, um erro inesperado ocorreu durante a solicitação da página pois alguma informação estava errada ou faltando. Já notificamos alguém para dar uma olhada.";
                    break;

                case 401:
                    model.Nome = "Acesso não autorizado";
                    model.Mensagem = "Você não tem permissão para visualizar esta página. Entre em contato com o Administrador do sistema.";
                    break;

                case 403:
                    model.Nome = "Acesso proibido";
                    model.Mensagem = "Solicitação ilegal!";
                    break;

                case 500:
                    model.Nome = "Erro Inesperado";
                    model.Mensagem = "Bom, isso é um tanto vergonhoso... Nossa aplicação sofreu um erro inesperado, mas não se preocupe, já estamos cientes do problema e um de nossos programadores irá resolvê-lo o mais rápido possível";
                    break;

                default:
                    model.HttpErrorCode = null;
                    model.Nome = "Erro Inesperado";
                    model.Mensagem = "Bom, isso é um tanto vergonhoso... Nossa aplicação sofreu um erro inesperado, mas não se preocupe, já estamos cientes do problema e um de nossos programadores irá resolvê-lo o mais rápido possível";
                    break;
            };

            return Json(model);
        }




        [Route("404")]
        public IActionResult PageNotFound()
        {

            var model = new GenericErrorPageViewModel
            {
                HttpErrorCode = 404,
                Nome = "Página não encontrada",
                Mensagem = "Oops! Pedimos desculpas, mas a página que você está procurando não pôde ser encontrada.",

            };
            return Json(model);
        }

    }

}

