﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using two_autenticao_jwt_And_cookies.Data;
using two_autenticao_jwt_And_cookies.Models;
using two_autenticao_jwt_And_cookies.ViewModels.Admin.UsersAdminViewModels;

namespace two_autenticao_jwt_And_cookies.Controllers.Admin
{
    public class UsersAdminController : BaseController
    {

        public UsersAdminController(AutenticacaoDataContext context) : base(context)
        {
        }

        [Route("login")]
        [HttpGet]
        [ResponseCache(NoStore = true)]
        public IActionResult Index(string returnUrl)
        {

            if (string.IsNullOrWhiteSpace(returnUrl) || returnUrl == "/")
            {
                returnUrl = null;
            }
            ViewBag.ReturnUrl = HttpUtility.HtmlEncode(returnUrl);

            var model = new LoginAdmViewModel()
            {
                UrlRetorno = returnUrl
            };



            return View("~/Views/Admin/Index.cshtml", model);
        }



        [Route("login")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ResponseCache(NoStore = true)]
        public async Task<IActionResult> AutenticarAdm(LoginAdmViewModel model)
        {
            model.ValidacaoBackend(this);

            if (ModelState.IsValid)
            {

                var usuario = db.Users.FirstOrDefault(us => us.Nome == model.Nome);

                var passwordHasher = new PasswordHasher<UserModel>();

                var hashedPassword = passwordHasher.VerifyHashedPassword(usuario, usuario.Password, model.Password);

                if (hashedPassword == PasswordVerificationResult.Success)
                {

                    var claims = new List<Claim>
                    {   //Salva no cookies o nome que será exibido na view
                        new Claim(ClaimTypes.Name, model.Nome.ToString()),
                                            new Claim(ClaimTypes.Role, usuario.Role.ToString())
                     };

                    var userIdentify = new ClaimsIdentity(claims, "login");
                    ClaimsPrincipal principal = new ClaimsPrincipal(userIdentify);
                    await HttpContext.SignInAsync(principal);

                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ModelState.AddModelError(nameof(model.Nome), "Usuário e/ou senha incorretos.");

                }




            }


            return View("~/Views/Admin/Index.cshtml", model);

        }

        [HttpGet]
        [ResponseCache(NoStore = true)]
        public async Task<IActionResult> Logout()
        {

            await HttpContext.SignOutAsync();

            return RedirectToAction(nameof(UsersAdminController.Index), "login");
        }

    }
}
